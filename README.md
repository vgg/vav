# Video Annotation Visualiser

A lightweight module to visualise bounding box annotations on videos. Immediate application will be other VGG projects (VIA, VVT, VDT). We envisage that this module will be useful for other projects as well.


## Usage

```
var html_video = document.getElementById('target-video');
var vav = new _via_video_annotation_visualizer(html_video);

// add annotations
vav.add_timestamp_bbox(1.324, [10,20,40,40], 'Fish 1');
vav.add_timestamp_bbox(1.724, [14,22,40,40], 'Fish 1');
vav.bulk_add_timestamp_bbox([3.456, 11.432, 20.676],
[[10,10,20,30],[...],[...]], ['Fish 3', 'Fish 2', 'Fish 9']);

// additional helper functions
//vav.update_timestamp_bbox(...)
//vav.reset_all_timestamp_bbox(...)
//vav.delete_timestamp_bbox(1.323)
//vav.disable() 
//vav.enable() 
```

That's it, nothing more needs to be done.
The `_via_video_annotation_visualizer` will automatically capture video
events and show bounding boxes and annotations when the video is playing.
